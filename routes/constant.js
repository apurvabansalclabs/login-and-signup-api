/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_DATA", 105);
define(exports.responseStatus, "EMAIL_NOT_REGISTERED", 106);
define(exports.responseStatus, "PASSWORD_INCORRECT", 107);
define(exports.responseStatus, "EMAIL_EXISTS", 108);
define(exports.responseStatus, "SHOW_LOGIN_DATA", 109);
define(exports.responseStatus, "SHOW_LOGGED_DATA", 110);
define(exports.responseStatus, "SHOW_LOGOUT_DATA", 111);


exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Registration Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGIN", "LOGIN Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGOUT", "LOGOUT Successful.");
define(exports.responseMessage, "ALREADY_LOGGED_IN", "You're already logged in");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered. Please sign up to login");
define(exports.responseMessage, "PASSWORD_INCORRECT", "Sorry, your password is incorrect");


