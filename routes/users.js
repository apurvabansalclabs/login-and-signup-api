var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var express = require('express');
var router = express.Router();
var generatePassword = require('password-generator');
var moment = require('moment');
var md5 = require('MD5');
var math = require('math');

var request = require("request");
var http = require("http");
var async = require("async");

router.post('/login', function (req, res) {
    var user = req.body.user;
    var pass = req.body.pass;
    console.log("user=" + user);
    console.log("pass=" + pass);
    var check = [user, pass];
    var flag = func.checkBlank(check);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        func.email_login_function(user, pass, res);

    }

});


router.post('/create', function (req, res) {
    var Fname = req.body.firstname;
    var Lname = req.body.lastname;
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var checkBlank = [Lname, Fname, email, password, phone];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);

        },
        function (callback) {
            func.checkEmailAvailability(res, email, callback);

        }], function (updatePopup) {
        var md5 = require('MD5');
        var hash = md5(password);
        var accesstoken = "helooo";
        var accesstoken1 = accesstoken + email;
        var accessToken = md5(accesstoken1);
        var loginTime = new Date();
        var sql = "INSERT into tb_users(Fname,Lname,email,encryptPassword,phone,access_token,date_registered,last_login) values(?,?,?,?,?,?,?,?)";
        connection.query(sql, [Fname, Lname, email, hash, phone, accessToken, loginTime,loginTime], function (err, result) {

            if (err) {
                console.log(err);
                sendResponse.somethingWentWrongError(res);
            }

            else {
                var sql = "SELECT Fname,Lname,email FROM tb_users WHERE email=? LIMIT 1";
                connection.query(sql, [email], function (err, user, fields) {


                    var final = {
                        "Fname": user[0].Fname,
                        "Lname": user[0].Lname,
                        "email": user[0].email,
                        "Password": user[0].encryptPassword,
                        "phone": user[0].phone,
                        "access_token": user[0].access_token,
                        "date_registered": user[0].date_registered
                    };
                    sendResponse.sendSuccessData(final, res);
                    var userName = user[0].Fname;
                    func.sendMailToCustomer(userName, email);
                });
            }
        });
    });
});

router.post('/logout', function (req, res, next) {

    var email = req.body.log;
    console.log(email);
    var sql = "SELECT `login_status` FROM `tb_users` WHERE `email`=? LIMIT 1";

    connection.query(sql, [email], function (err, result) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {

            var sql = "UPDATE `tb_users` SET `login_status`=? WHERE `email`=? LIMIT 1";
            connection.query(sql, [0, email], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    sendResponse.sendSuccessLogout(res);
                }
            });
        }
    });
});

module.exports = router;