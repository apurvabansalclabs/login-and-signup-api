var sendResponse = require('./sendResponse');
var config = require('config');


exports.checkBlank = function (arr) {

    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === '' || arr[i] === undefined || arr[i] === '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
};
exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {
           
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            
            sendResponse.emailExists(res);
        }
        else {
            callback();
        }
    });
}
exports.email_login_function = function (email, password, res) {
    var sql = "SELECT `login_status` FROM `tb_users` WHERE `email`=? LIMIT 1";
    connection.query(sql, [email], function (err, result) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (result[0].login_status === 1) {
            sendResponse.alreadyLoggedIn(res);
        }
        else {
            var md5 = require('MD5');
            var hash = md5(password);
            var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? LIMIT 1";
            connection.query(sql, [email], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);

                }
                else {
                    var length2 = result.length;
                    if (length2 == 0) {

                        sendResponse.emailNotRegistered(res);
                    }
                    else {
                        var user = result[0].user_id;

                        var sql = "SELECT `access_token`,`phone`,`user_id`,`Fname`\n\
             FROM `tb_users` WHERE `user_id`=? && `encryptPassword`=? LIMIT 1"
                        connection.query(sql, [user, hash], function (err, result_user) {
                            if (err) {

                                sendResponse.somethingWentWrongError(res);

                            }
                            else {
                                console.log(result_user);
                                var result_user_length = result_user.length;
                                if (result_user_length === 0) {

                                    sendResponse.passwordIncorrect(res);
                                }
                                else {
                                    sendResponse.successLogin(result_user, res);
                                }
                            }
                        });
                    }
                }
            });
        }

    });


}
exports.sendMailToCustomer = function (userName, email) {
    var toEmail = email;
    var sub = "Welcome To Clicklabs";
    var html = '<html>';
    html += '<head>';
    html += '<title>congratulations</title>';
    html += '</head>';
    html += '<body>';


    html += '<p>You are successfully registered</p>';

    html += '</body>';
    html += '</html>';
    sendEmail(toEmail, html, sub, function (result) {
        if (result === 1) {
            console.log("Mail Sent");

        }
        else {
            console.log("Error Sending Mail");
        }
    });


};

function sendEmail(receiverMailId, message, subject, callback) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password

        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message

    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, info) {

        if (error) {
            return callback(0);
            console.log(error);
        } else {
            return callback(1);
            console.log('Message sent');
        }
    });
}
;


